package br.com.btsoftwares.apps.controller;

import java.util.List;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class CadastroPedidosBean {

	private List<Integer> itens;

	public CadastroPedidosBean() {
		itens = new ArrayList<>(); 
		itens.add(1);
	}

	public List<Integer> getItens() {
		return itens;
	}
}
